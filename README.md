# beelzebub.el

## Archved
This project is now archived and the theme lives at
[colourless-themes](https://gitlab.com/peregrinator/colourless-themes-el)


An Emacs port of Stanislaw Mnizhek's vim theme
[vim-beelzebub](https://github.com/xdefrag/vim-beelzebub). This now uses the
[`colorless-themes`](https://git.sr.ht/~lthms/colorless-themes.el) 
macro and it will have to be installed separately for this to work properly.

## Screenshot(s)

![scrot](screenshots/scrot_1.jpg)

## Terminal colours (WIP)

Also included is a terminal colour theme with nearly identical colours 
